module test

go 1.19

require (
	github.com/lib/pq v1.10.7
	github.com/rubenv/sql-migrate v1.2.0
	github.com/spf13/cobra v1.5.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/go-gorp/gorp/v3 v3.0.2 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
