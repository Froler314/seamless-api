package cmd

import (
	"errors"
	"strconv"

	"github.com/spf13/cobra"

	"test/pkg/postgres"
)

var (
	migrateCmd = &cobra.Command{
		Use:   "migrate [direction]",
		Short: "Migrate database",
	}

	migrateUpCmd = &cobra.Command{
		Use:   "up",
		Short: "Migrate up database",
		RunE: func(_ *cobra.Command, _ []string) error {
			var migrator = postgres.NewMigrator(cfg.Postgres)
			return migrator.MigrateUp()
		},
	}

	migrateDownCmd = &cobra.Command{
		Use:   "down [limit]",
		Short: "Rollback migration",
		Args: func(_ *cobra.Command, args []string) error {
			var (
				limit int
				err   error
			)
			if limit, err = strconv.Atoi(args[0]); err != nil {
				return err
			}

			if limit < 0 {
				return errors.New("incorrect migrations limit")
			}

			return nil
		},
		RunE: func(_ *cobra.Command, args []string) error {
			var limit int
			limit, _ = strconv.Atoi(args[0])

			var migrator = postgres.NewMigrator(cfg.Postgres)

			return migrator.MigrateDown(limit)
		},
	}
)

func init() {
	migrateCmd.AddCommand(migrateUpCmd)
	migrateCmd.AddCommand(migrateDownCmd)
	rootCmd.AddCommand(migrateCmd)
}
