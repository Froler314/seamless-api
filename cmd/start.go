package cmd

import (
	"context"

	"github.com/spf13/cobra"

	"test/cmd/http"
	"test/cmd/http/handlers"
	"test/cmd/http/handlers/seamless"
)

func init() {
	rootCmd.AddCommand(&cobra.Command{
		Use:   "start",
		Short: "Start service",
		RunE: func(cmd *cobra.Command, args []string) error {
			// graceful shutdown
			var ctx, cancel = context.WithCancel(context.Background())
			go func() {
				defer cancel()
				<-stopNotification
			}()

			return http.Run(ctx, cfg.HTTP, map[string]handlers.Handler{
				"/api": seamless.NewSeamless(ctx, db),
			})
		},
	})
}
