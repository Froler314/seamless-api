package cmd

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"test/cmd/http"
	"test/pkg/postgres"
)

var (
	cfgPath string
	cfg     Config
	db      postgres.Database

	stopNotification = make(chan struct{})

	rootCmd = &cobra.Command{
		Use:           "app [command]",
		Long:          "app project",
		SilenceUsage:  true,
		SilenceErrors: true,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
			var cfgYaml = make([]byte, 0)
			if cfgYaml, err = os.ReadFile(cfgPath); err != nil {
				return err
			}

			if err = yaml.Unmarshal(cfgYaml, &cfg); err != nil {
				return err
			}

			if db, err = postgres.NewDatabase(cfg.Postgres); err != nil {
				return err
			}

			// graceful stop
			go func() {
				var c = make(chan os.Signal, 1)
				signal.Notify(c,
					syscall.SIGHUP,
					syscall.SIGINT,
					syscall.SIGTERM,
				)

				<-c

				stopNotification <- struct{}{}
			}()

			return nil
		},
	}
)

type Config struct {
	HTTP     *http.Config     `yaml:"http"`
	Postgres *postgres.Config `yaml:"postgres"`
}

func Run() error {
	rootCmd.PersistentFlags().StringVarP(&cfgPath, "config", "c", "config.yaml", "path to config file")
	if err := rootCmd.Execute(); err != nil {
		return err
	}

	if err := db.Close(); err != nil {
		return err
	}

	return nil
}
