package handlers

import (
	"net/http"
)

type Handler interface {
	GetHttpHandler() http.Handler
}

func WriteNotFound(resp http.ResponseWriter) {
	resp.WriteHeader(404)
	_, _ = resp.Write([]byte("Not Found"))
}

func WriteBadRequest(resp http.ResponseWriter, err error) {
	resp.WriteHeader(400)
	_, _ = resp.Write([]byte("Bad request: " + err.Error()))
}

func WriteInternalError(resp http.ResponseWriter, err error) {
	resp.WriteHeader(500)
	_, _ = resp.Write([]byte("Internal error: " + err.Error()))
}
