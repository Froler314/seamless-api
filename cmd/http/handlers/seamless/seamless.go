package seamless

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"

	"test/cmd/http/handlers"
	"test/pkg/postgres"
	"test/pkg/repository"
)

const (
	errNotEnoughMoneyCode = iota + 1
	errIllegalCurrencyCode
	errNegativeDepositCode
	errNegativeWithdrawalCode
	errSpendingBudgetExceeded
	errTransactionWasAlreadyRolledBack
	errUnknownPlayer
)

var (
	errorMap = map[int]*ResponseError{
		errNotEnoughMoneyCode:              {Code: errNotEnoughMoneyCode, Message: "ErrNotEnoughMoneyCode"},
		errIllegalCurrencyCode:             {Code: errIllegalCurrencyCode, Message: "ErrIllegalCurrencyCode"},
		errNegativeDepositCode:             {Code: errNegativeDepositCode, Message: "ErrNegativeDepositCode"},
		errNegativeWithdrawalCode:          {Code: errNegativeWithdrawalCode, Message: "ErrNegativeWithdrawalCode"},
		errSpendingBudgetExceeded:          {Code: errSpendingBudgetExceeded, Message: "ErrSpendingBudgetExceeded"},
		errTransactionWasAlreadyRolledBack: {Code: errTransactionWasAlreadyRolledBack, Message: "ErrTransactionWasAlreadyRolledBack"},
		errUnknownPlayer:                   {Code: errUnknownPlayer, Message: "ErrUnknownPlayer"},
	}

	errPlayerNotFound  = errors.New("player not found")
	errCurrencyInvalid = errors.New("currency is invalid")
)

type (
	Request struct {
		JsonRpc string `json:"jsonrpc"`
		Method  string `json:"method"`
		Id      int64  `json:"id"`
	}

	Response struct {
		JsonRpc string         `json:"jsonrpc"`
		Id      int64          `json:"id"`
		Error   *ResponseError `json:"error,omitempty"`
	}
	ResponseError struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}

	seamless struct {
		ctx                   context.Context
		tm                    postgres.TransactionManager
		playerRepository      repository.PlayerRepository
		transactionRepository repository.TransactionRepository
	}
)

func NewSeamless(ctx context.Context, db postgres.Database) handlers.Handler {
	return &seamless{
		ctx:                   ctx,
		tm:                    postgres.NewTransactionManager(db),
		playerRepository:      repository.NewPlayerRepository(db),
		transactionRepository: repository.NewTransactionRepository(db),
	}
}

func (h *seamless) GetHttpHandler() http.Handler {
	return http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		if req.Method != "POST" {
			handlers.WriteNotFound(resp)
			return
		}

		var (
			jsonContent []byte
			err         error
		)
		if jsonContent, err = io.ReadAll(req.Body); err != nil {
			handlers.WriteInternalError(resp, err)
			return
		}

		var (
			request = &Request{}
		)
		if err = json.Unmarshal(jsonContent, request); err != nil {
			handlers.WriteBadRequest(resp, err)
			return
		}

		if request == nil {
			handlers.WriteBadRequest(resp, fmt.Errorf("cannot parse json request"))
			return
		}

		var responseContent = make([]byte, 0)
		switch request.Method {
		case "getBalance":
			var getBalanceRequest = &GetBalanceRequest{}
			if err = json.Unmarshal(jsonContent, getBalanceRequest); err != nil {
				handlers.WriteBadRequest(resp, err)
				return
			}

			var response *GetBalanceResponse
			if response, err = h.getBalance(getBalanceRequest); err != nil {
				handlers.WriteInternalError(resp, err)
				return
			}

			if responseContent, err = json.Marshal(response); err != nil {
				handlers.WriteInternalError(resp, err)
				return
			}
		case "withdrawAndDeposit":
			var withdrawAndDepositRequest = &WithdrawAndDepositRequest{}
			if err = json.Unmarshal(jsonContent, withdrawAndDepositRequest); err != nil {
				handlers.WriteBadRequest(resp, err)
				return
			}

			var response *WithdrawAndDepositResponse
			if response, err = h.withdrawAndDeposit(withdrawAndDepositRequest); err != nil {
				handlers.WriteInternalError(resp, err)
				return
			}

			if responseContent, err = json.Marshal(response); err != nil {
				handlers.WriteInternalError(resp, err)
				return
			}
		case "rollbackTransaction":
			var rollbackTransactionRequest = &RollbackTransactionRequest{}
			if err = json.Unmarshal(jsonContent, rollbackTransactionRequest); err != nil {
				handlers.WriteBadRequest(resp, err)
				return
			}

			var response *RollbackTransactionResponse
			if response, err = h.rollbackTransaction(rollbackTransactionRequest); err != nil {
				handlers.WriteInternalError(resp, err)
				return
			}

			if responseContent, err = json.Marshal(response); err != nil {
				handlers.WriteInternalError(resp, err)
				return
			}
		default:
			handlers.WriteBadRequest(resp, fmt.Errorf("unknown method"))
			return
		}

		resp.Header().Add("Content-Type", "application/json")
		_, _ = resp.Write(responseContent)
	})
}
