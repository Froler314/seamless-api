package seamless

import (
	"database/sql"
	"errors"

	"test/pkg/model"
)

type (
	WithdrawAndDepositRequestParams struct {
		CallerId             int     `json:"callerId"`
		PlayerName           string  `json:"playerName"`
		Withdraw             int64   `json:"withdraw"`
		Deposit              int64   `json:"deposit"`
		Currency             string  `json:"currency"`
		TransactionRef       string  `json:"transactionRef"`
		GameRoundRef         *string `json:"gameRoundRef"`
		GameId               *string `json:"gameId"`
		Reason               *string `json:"reason"`
		SessionId            *string `json:"sessionId"`
		SessionAlternativeId *string `json:"sessionAlternativeId"`
		ChargeFreerounds     *int    `json:"chargeFreerounds"`
		BonusId              *string `json:"bonusId"`
		SpinDetails          *struct {
			BetType string `json:"betType"`
			WinType string `json:"winType"`
		} `json:"spinDetails"`
	}
	WithdrawAndDepositRequest struct {
		Request
		Params WithdrawAndDepositRequestParams `json:"params"`
	}

	WithdrawAndDepositResponse struct {
		Response
		Result *WithdrawAndDepositResponseResult `json:"result,omitempty"`
	}
	WithdrawAndDepositResponseResult struct {
		NewBalance    int64  `json:"newBalance"`
		TransactionId string `json:"transactionId"`
	}
)

func (h *seamless) withdrawAndDeposit(r *WithdrawAndDepositRequest) (*WithdrawAndDepositResponse, error) {
	if r.Params.Withdraw < 0 {
		return &WithdrawAndDepositResponse{
			Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errNegativeWithdrawalCode]},
		}, nil
	}
	if r.Params.Deposit < 0 {
		return &WithdrawAndDepositResponse{
			Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errNegativeDepositCode]},
		}, nil
	}

	var (
		exists bool
		err    error
	)
	if exists, err = h.transactionRepository.IsExistsTransaction(h.ctx, r.Params.TransactionRef); err != nil {
		return nil, err
	}
	if exists {
		var player *model.Player
		if player, err = h.playerRepository.Find(h.ctx, r.Params.PlayerName); err != nil {
			return nil, err
		}

		if player == nil {
			return &WithdrawAndDepositResponse{
				Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errUnknownPlayer]},
			}, nil
		}

		if player.Currency != r.Params.Currency {
			return &WithdrawAndDepositResponse{
				Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errIllegalCurrencyCode]},
			}, nil
		}

		return &WithdrawAndDepositResponse{
			Response: Response{JsonRpc: r.JsonRpc, Id: r.Id},
			Result: &WithdrawAndDepositResponseResult{
				NewBalance:    player.Balance,
				TransactionId: r.Params.TransactionRef,
			},
		}, nil
	}

	if exists, err = h.transactionRepository.IsExistsRollbackTransaction(h.ctx, r.Params.TransactionRef); err != nil {
		return nil, err
	}
	if exists {
		return &WithdrawAndDepositResponse{
			Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errTransactionWasAlreadyRolledBack]},
		}, nil
	}

	var newBalance int64
	if err = h.tm.Transactional(func(tx *sql.Tx) error {
		var player *model.Player
		if player, err = h.playerRepository.FindForUpdate(h.ctx, tx, r.Params.PlayerName); err != nil {
			return err
		}

		if player == nil {
			return errPlayerNotFound
		}

		if player.Currency != r.Params.Currency {
			return errCurrencyInvalid
		}

		var delta = r.Params.Deposit
		if r.Params.Withdraw > 0 {
			delta = -r.Params.Withdraw
		}

		if newBalance, err = h.playerRepository.ChangeBalanceTx(h.ctx, tx, player.Name, delta); err != nil {
			return err
		}

		if err = h.transactionRepository.AddTx(h.ctx, tx, &model.Transaction{
			PlayerName:     r.Params.PlayerName,
			TransactionRef: r.Params.TransactionRef,
			Value:          delta,
		}); err != nil {
			return err
		}

		return nil
	}); err != nil {
		if errors.Is(err, errPlayerNotFound) {
			return &WithdrawAndDepositResponse{
				Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errUnknownPlayer]},
			}, nil
		}
		if errors.Is(err, errCurrencyInvalid) {
			return &WithdrawAndDepositResponse{
				Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errIllegalCurrencyCode]},
			}, nil
		}

		return nil, err
	}

	return &WithdrawAndDepositResponse{
		Response: Response{JsonRpc: r.JsonRpc, Id: r.Id},
		Result: &WithdrawAndDepositResponseResult{
			NewBalance:    newBalance,
			TransactionId: r.Params.TransactionRef,
		},
	}, nil
}
