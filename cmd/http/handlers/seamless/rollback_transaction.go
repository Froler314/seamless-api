package seamless

import (
	"database/sql"
	"errors"

	"test/pkg/model"
)

type (
	RollbackTransactionRequestParams struct {
		CallerId             int    `json:"callerId"`
		PlayerName           string `json:"playerName"`
		TransactionRef       string `json:"transactionRef"`
		GameId               string `json:"gameId"`
		SessionId            string `json:"sessionId"`
		SessionAlternativeId string `json:"sessionAlternativeId"`
	}
	RollbackTransactionRequest struct {
		Request
		Params RollbackTransactionRequestParams `json:"params"`
	}

	RollbackTransactionResponse struct {
		Response
		Result *RollbackTransactionResponseResult `json:"result,omitempty"`
	}
	RollbackTransactionResponseResult struct{}
)

func (h *seamless) rollbackTransaction(r *RollbackTransactionRequest) (*RollbackTransactionResponse, error) {
	var (
		exists bool
		err    error
	)
	if exists, err = h.transactionRepository.IsExistsRollbackTransaction(h.ctx, r.Params.TransactionRef); err != nil {
		return nil, err
	}

	if exists {
		return &RollbackTransactionResponse{
			Response: Response{JsonRpc: r.JsonRpc, Id: r.Id},
			Result:   &RollbackTransactionResponseResult{},
		}, nil
	}

	if err = h.tm.Transactional(func(tx *sql.Tx) (err error) {
		var player *model.Player
		if player, err = h.playerRepository.FindForUpdate(h.ctx, tx, r.Params.PlayerName); err != nil {
			return err
		}

		if player == nil {
			return errPlayerNotFound
		}

		var origTransaction *model.Transaction
		if origTransaction, err = h.transactionRepository.FindTx(
			h.ctx,
			tx,
			r.Params.TransactionRef,
			r.Params.PlayerName,
		); err != nil {
			return err
		}

		if origTransaction != nil {
			if _, err = h.playerRepository.ChangeBalanceTx(h.ctx, tx, player.Name, -origTransaction.Value); err != nil {
				return err
			}
		}

		if err = h.transactionRepository.AddTx(h.ctx, tx, &model.Transaction{
			PlayerName:     r.Params.PlayerName,
			TransactionRef: r.Params.TransactionRef,
			Value:          0,
			IsRollback:     true,
		}); err != nil {
			return err
		}

		return nil
	}); err != nil {
		if errors.Is(err, errPlayerNotFound) {
			return &RollbackTransactionResponse{
				Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errUnknownPlayer]},
			}, nil
		}
	}

	return &RollbackTransactionResponse{
		Response: Response{JsonRpc: r.JsonRpc, Id: r.Id},
		Result:   &RollbackTransactionResponseResult{},
	}, nil
}
