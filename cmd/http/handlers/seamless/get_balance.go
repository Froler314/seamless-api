package seamless

import (
	"test/pkg/model"
)

type (
	GetBalanceRequestParams struct {
		CallerId             int64   `json:"callerId"`
		PlayerName           string  `json:"playerName"`
		Currency             string  `json:"currency"`
		GameId               *string `json:"gameId"`
		SessionId            *string `json:"sessionId"`
		SessionAlternativeId *string `json:"sessionAlternativeId"`
		BonusId              *string `json:"bonusId"`
	}
	GetBalanceRequest struct {
		Request
		Params GetBalanceRequestParams `json:"params"`
	}

	GetBalanceResponse struct {
		Response
		Result *GetBalanceResponseResult `json:"result,omitempty"`
	}
	GetBalanceResponseResult struct {
		Balance        int64 `json:"balance"`
		FreeRoundsLeft *int  `json:"freeroundsLeft,omitempty"`
	}
)

func (h *seamless) getBalance(r *GetBalanceRequest) (_ *GetBalanceResponse, err error) {
	var player = &model.Player{
		Name:     r.Params.PlayerName,
		Balance:  model.DefaultBalance,
		Currency: r.Params.Currency,
	}
	if err = h.playerRepository.AddIfNotExists(h.ctx, player); err != nil {
		return nil, err
	}
	if player, err = h.playerRepository.Find(h.ctx, r.Params.PlayerName); err != nil {
		return nil, err
	}

	if player == nil || player.Currency != r.Params.Currency {
		return &GetBalanceResponse{
			Response: Response{JsonRpc: r.JsonRpc, Id: r.Id, Error: errorMap[errIllegalCurrencyCode]},
		}, nil
	}

	return &GetBalanceResponse{
		Response: Response{JsonRpc: r.JsonRpc, Id: r.Id},
		Result:   &GetBalanceResponseResult{Balance: player.Balance},
	}, nil
}
