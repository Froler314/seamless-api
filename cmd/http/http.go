package http

import (
	"context"
	"errors"
	"log"
	"net/http"

	"test/cmd/http/handlers"
)

type (
	Config struct {
		Listen string `yaml:"listen"`
	}
)

func Run(ctx context.Context, cfg *Config, handlers map[string]handlers.Handler) error {
	var mux = http.NewServeMux()
	for route, handler := range handlers {
		mux.Handle(route, handler.GetHttpHandler())
	}

	var server = &http.Server{
		Addr:    cfg.Listen,
		Handler: mux,
	}

	var errChan = make(chan error)
	go func() {
		log.Println("Start listen HTTP...")
		if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			errChan <- err
		}
	}()

	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
		if err := server.Close(); err != nil {
			return err
		}
		log.Println("Stop listen HTTP...")
	}

	return nil
}
