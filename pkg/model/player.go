package model

const DefaultBalance = 10000 // 100 уе

type Player struct {
	Name     string
	Balance  int64
	Currency string
}
