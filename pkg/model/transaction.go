package model

type Transaction struct {
	Id             int64
	PlayerName     string
	TransactionRef string
	Value          int64
	IsRollback     bool
}
