package postgres

import (
	"database/sql"
	"fmt"
	"sync"

	_ "github.com/lib/pq" // database driver
)

type (
	Config struct {
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Database string `yaml:"database"`
	}

	Database interface {
		DB() *sql.DB
		Close() error
	}

	postgres struct {
		conn *sql.DB
		lock *sync.RWMutex
	}
)

func NewDatabase(cfg *Config) (Database, error) {
	var (
		db  *sql.DB
		err error
	)
	if db, err = sql.Open("postgres", getUrl(cfg.Host, cfg.Port, cfg.Username, cfg.Password, cfg.Database)); err != nil {
		return nil, err
	}

	return &postgres{conn: db, lock: &sync.RWMutex{}}, nil
}

func (p *postgres) DB() *sql.DB {
	p.lock.RLock()
	defer p.lock.RUnlock()
	return p.conn
}

func (p *postgres) Close() error {
	return p.DB().Close()
}

func getUrl(host, port, username, password, database string) string {
	return fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		host,
		port,
		username,
		database,
		password,
	)
}
