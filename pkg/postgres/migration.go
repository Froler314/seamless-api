package postgres

import (
	"database/sql"

	_ "github.com/lib/pq" // database driver
	migrate "github.com/rubenv/sql-migrate"
)

type (
	Migrator interface {
		MigrateUp() error
		MigrateDown(limit int) error
	}
	migrator struct {
		dsn        string
		migrations *migrate.FileMigrationSource
	}
)

func NewMigrator(cfg *Config) Migrator {
	return &migrator{
		dsn:        getUrl(cfg.Host, cfg.Port, cfg.Username, cfg.Password, cfg.Database),
		migrations: &migrate.FileMigrationSource{Dir: "migrations"},
	}
}

func (m *migrator) MigrateUp() error {
	return m.doMigrate(migrate.Up, 0)
}

func (m *migrator) MigrateDown(limit int) error {
	return m.doMigrate(migrate.Down, limit)
}

func (m *migrator) doMigrate(direction migrate.MigrationDirection, limit int) error {
	var (
		db  *sql.DB
		err error
	)
	if db, err = sql.Open("postgres", m.dsn); err != nil {
		return err
	}

	defer func() { _ = db.Close() }()

	if _, err = migrate.ExecMax(db, "postgres", m.migrations, direction, limit); err != nil {
		return err
	}

	return nil
}
