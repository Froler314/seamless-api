package postgres

import (
	"database/sql"
)

type (
	TransactionManager interface {
		Transactional(fn func(tx *sql.Tx) error) error
	}
	transactionManager struct {
		db Database
	}
)

func NewTransactionManager(db Database) TransactionManager {
	return &transactionManager{db: db}
}

func (tm *transactionManager) Transactional(fn func(tx *sql.Tx) error) error {
	var (
		tx  *sql.Tx
		err error
	)
	if tx, err = tm.db.DB().Begin(); err != nil {
		return err
	}

	if err = fn(tx); err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}

		return err
	}

	return tx.Commit()
}
