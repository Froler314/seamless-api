package repository

import (
	"context"
	"database/sql"
	"errors"

	"test/pkg/model"
	"test/pkg/postgres"
)

type (
	PlayerRepository interface {
		AddIfNotExists(ctx context.Context, player *model.Player) error
		Find(ctx context.Context, name string) (*model.Player, error)
		FindForUpdate(ctx context.Context, tx *sql.Tx, name string) (*model.Player, error)
		ChangeBalanceTx(ctx context.Context, tx *sql.Tx, name string, delta int64) (int64, error)
	}
	playerRepository struct {
		db postgres.Database
	}
)

func NewPlayerRepository(db postgres.Database) PlayerRepository {
	return &playerRepository{db: db}
}

func (r *playerRepository) AddIfNotExists(ctx context.Context, player *model.Player) error {
	var _, err = r.db.DB().ExecContext(
		ctx,
		"INSERT INTO players (name, balance, currency) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING",
		player.Name,
		player.Balance,
		player.Currency,
	)

	return err
}

func (r *playerRepository) Find(ctx context.Context, name string) (*model.Player, error) {
	var player = &model.Player{}
	if err := r.db.DB().QueryRowContext(
		ctx,
		"SELECT name, balance, currency FROM players WHERE name = $1",
		name,
	).Scan(
		&player.Name,
		&player.Balance,
		&player.Currency,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return player, nil
}

func (r *playerRepository) FindForUpdate(ctx context.Context, tx *sql.Tx, name string) (*model.Player, error) {
	var player = &model.Player{}
	if err := tx.QueryRowContext(
		ctx,
		"SELECT name, balance, currency FROM players WHERE name = $1 FOR UPDATE",
		name,
	).Scan(
		&player.Name,
		&player.Balance,
		&player.Currency,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return player, nil
}

func (r *playerRepository) ChangeBalanceTx(ctx context.Context, tx *sql.Tx, name string, delta int64) (int64, error) {
	var newBalance int64
	if err := tx.QueryRowContext(
		ctx,
		"UPDATE players SET balance = balance + $1 WHERE name = $2 RETURNING balance",
		delta,
		name,
	).Scan(&newBalance); err != nil {
		return 0, err
	}

	return newBalance, nil
}
