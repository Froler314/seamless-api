package repository

import (
	"context"
	"database/sql"
	"errors"

	"test/pkg/model"
	"test/pkg/postgres"
)

type (
	TransactionRepository interface {
		AddTx(ctx context.Context, tx *sql.Tx, transaction *model.Transaction) error
		FindTx(ctx context.Context, tx *sql.Tx, transactionRef, playerName string) (*model.Transaction, error)
		IsExistsTransaction(ctx context.Context, transactionRef string) (bool, error)
		IsExistsRollbackTransaction(ctx context.Context, transactionRef string) (bool, error)
	}
	transactionRepository struct {
		db postgres.Database
	}
)

func NewTransactionRepository(db postgres.Database) TransactionRepository {
	return &transactionRepository{db: db}
}

func (r *transactionRepository) AddTx(ctx context.Context, tx *sql.Tx, transaction *model.Transaction) error {
	var _, err = tx.ExecContext(
		ctx,
		`
			INSERT INTO transactions (player_name, transaction_ref, value, is_rollback)
			VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING`,
		transaction.PlayerName,
		transaction.TransactionRef,
		transaction.Value,
		transaction.IsRollback,
	)

	return err

}

func (r *transactionRepository) FindTx(ctx context.Context, tx *sql.Tx, transactionRef, playerName string) (*model.Transaction, error) {
	var transaction = &model.Transaction{}
	if err := tx.QueryRowContext(
		ctx,
		`
			SELECT id, player_name, transaction_ref, value, is_rollback
			FROM transactions
			WHERE transaction_ref = $1 AND player_name = $2
		`,
		transactionRef,
		playerName,
	).Scan(
		&transaction.Id,
		&transaction.PlayerName,
		&transaction.TransactionRef,
		&transaction.Value,
		&transaction.IsRollback,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}

	return transaction, nil
}

func (r *transactionRepository) IsExistsTransaction(ctx context.Context, transactionRef string) (bool, error) {
	var exists bool
	if err := r.db.DB().QueryRowContext(
		ctx,
		"SELECT EXISTS(SELECT id FROM transactions WHERE transaction_ref = $1 AND is_rollback = FALSE)",
		transactionRef,
	).Scan(&exists); err != nil {
		return false, err
	}

	return exists, nil
}

func (r *transactionRepository) IsExistsRollbackTransaction(ctx context.Context, transactionRef string) (bool, error) {
	var exists bool
	if err := r.db.DB().QueryRowContext(
		ctx,
		"SELECT EXISTS(SELECT id FROM transactions WHERE transaction_ref = $1 AND is_rollback = TRUE)",
		transactionRef,
	).Scan(&exists); err != nil {
		return false, err
	}

	return exists, nil
}
