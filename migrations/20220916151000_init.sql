-- +migrate Up
CREATE TABLE IF NOT EXISTS players (
    name TEXT NOT NULL PRIMARY KEY,
    balance BIGINT NOT NULL DEFAULT 0,
    currency CHAR(3) NOT NULL DEFAULT 'EUR'
);

CREATE TABLE IF NOT EXISTS transactions (
    id BIGSERIAL PRIMARY KEY,
    player_name TEXT NOT NULL REFERENCES players(name) ON DELETE RESTRICT ON UPDATE CASCADE,
    transaction_ref TEXT NOT NULL,
    value BIGINT,
    is_rollback BOOLEAN NOT NULL
);
CREATE UNIQUE INDEX transactions_transaction_ref_is_rollback ON transactions(transaction_ref, is_rollback);

-- +migrate Down
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS player;