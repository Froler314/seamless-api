# Seamless API client

## Install And Run

```bash
$ docker-compose -p seamless up -d
```

## Configuration

Default config file is `config.yaml`:
```yaml
http:
  listen: ":3001"

postgres:
  host: db
  port: 5432
  username: seamless
  password: seamless
  database: seamless
```

## Cli

### Migrations

Migrate up:

```bash
$ app migrate up
```

Migrate down:

```bash
$ app migrate down [limit]
```

### Start service

```bash
$ app start
```