package main

import (
	"log"

	"test/cmd"
)

func main() {
	if err := cmd.Run(); err != nil {
		log.Fatal(err.Error())
	}
}
